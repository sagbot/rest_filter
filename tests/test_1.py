from rest_filter.encoder.common import (
    encoder,
    And, Or,
    Equals, NotEquals,
    GraterThan, GraterEqualsTo,
    LessThan, LessEqualsTo
)
from rest_filter.encoder import Field


def test_encode_equals():
    expression = "(name eq \"string\")"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = Equals(field=Field(name='name'), value='string')

    assert encoded_expression == expected_encoded_expression


def test_encode_not_equals():
    expression = "(name ne \"string\")"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = NotEquals(field=Field(name='name'), value='string')

    assert encoded_expression == expected_encoded_expression


def test_encode_grater_than():
    expression = "(age gt 1)"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = GraterThan(field=Field(name='age'), value=1)

    assert encoded_expression == expected_encoded_expression


def test_encode_grater_equals_to():
    expression = "(age ge 1)"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = GraterEqualsTo(field=Field(name='age'), value=1)

    assert encoded_expression == expected_encoded_expression


def test_encode_less_than():
    expression = "(age lt 1)"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = LessThan(field=Field(name='age'), value=1)

    assert encoded_expression == expected_encoded_expression


def test_encode_less_equals_to():
    expression = "(age le 1)"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = LessEqualsTo(field=Field(name='age'), value=1)

    assert encoded_expression == expected_encoded_expression


def test_encode_number():
    expression = "(age eq 1)"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = Equals(field=Field(name='age'), value=1)

    assert encoded_expression == expected_encoded_expression


def test_encode_string_1():
    expression = '(name eq "eric")'
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = Equals(field=Field(name='name'), value='eric')

    assert encoded_expression == expected_encoded_expression


def test_encode_string_2():
    expression = "(name eq 'eric')"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = Equals(field=Field(name='name'), value='eric')

    assert encoded_expression == expected_encoded_expression


def test_encode_boolean_true():
    expression = "(male eq true)"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = Equals(field=Field(name='male'), value=True)

    assert encoded_expression == expected_encoded_expression


def test_encode_boolean_false():
    expression = "(male eq false)"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = Equals(field=Field(name='male'), value=False)

    assert encoded_expression == expected_encoded_expression


def test_encode_and():
    expression = "((age le 1) and (age le 1))"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = And(operands=[
        LessEqualsTo(field=Field(name='age'), value=1),
        LessEqualsTo(field=Field(name='age'), value=1)])

    assert encoded_expression == expected_encoded_expression


def test_encode_or():
    expression = "((age le 1) or (age le 1))"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = Or(operands=[
        LessEqualsTo(field=Field(name='age'), value=1),
        LessEqualsTo(field=Field(name='age'), value=1)])

    assert encoded_expression == expected_encoded_expression


def test_encode_1():
    expression = "((age eq \"Kaka\") and (age eq '10'))"
    encoded_expression = encoder.encode_expression(expression)
    expected_encoded_expression = And(operands=[
        Equals(field=Field(name='age'), value='Kaka'),
        Equals(field=Field(name='age'), value='10'),
    ])

    assert encoded_expression == expected_encoded_expression
